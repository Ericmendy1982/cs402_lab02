        .data 0x10010000
var1:    .word 51               # var1 is an integer (32 bit) with the ..
var2:  .word 12

            
      .extern ext1 4   
      .extern ext2 4    


        .text
        .globl main

main:       addu $s0, $ra, $0      # save $31 in $16
            lw $t1,var1         # load var1 to $t1
            sw $t1,ext1         # copy $t1 to ext1
            lw $t2,var2         # load var2 to $t2
            sw $t2,ext2         # copy $t2 to ext2
            
             
            li $v0,1            # print ext1
            lw $a0,ext1
            syscall
            
            
            
            li $v0,1            # print ext2  
            lw $a0,ext2
            syscall

            
# restore now the return address in $ra and return from main
            addu $ra, $0, $s0       # return address back in $31
            jr $ra                  # return from main
