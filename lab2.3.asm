         .data 0x10010000
var1:    .byte 0x10               # var1 is an integer (8 bit) with the ..
var2:    .byte 0x20
var3:   .byte 0x30
var4:   .byte 0x40         
first: .byte 'H'           # initial letter of first name, a byte(8bit)
last:  .byte 'Y'           # initial letter of last name,a byte (8 bit)

        .text
        .globl main
main:       addu $s0, $ra, $0      # save $31 in $16
            lui $1,4097         # load var1 to $t1
            lb $9,0($1)
             lb $10,1($1)                   # load var2 to $t2
            
            lb $11,2($1)         # load var3 to $t3

            lb $12,3($1)        # load var4 to $t4

            ori $13,$1,4        # load first to $t5

            ori $14,$1,5        # load last to $t6

            sb $9,3($1)         # swap var1 & var 4
            sb $12,0($1)
            sb $10,2($1)         # swap var2 & var3
            sb $11,1($1)
             

            
# restore now the return address in $ra and return from main
            addu $ra, $0, $s0       # return address back in $31
            jr $ra                  # return from main
