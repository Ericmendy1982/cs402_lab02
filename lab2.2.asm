         .data 0x10010000
var1:    .word 0x10               # var1 is an integer (32 bit) with the ..
var2:    .word 0x20
var3:   .word 0x30
var4:   .word 0x40         
first: .byte 'H'           # initial letter of first name, a byte(8bit)
last:  .byte 'Y'           # initial letter of last name,a byte (8 bit)

        .text
        .globl main
main:       addu $s0, $ra, $0      # save $31 in $16
            lw $t1,var1         # load var1 to $t1
            lw $t2,var2         # load var2 to $t2
            lw $t3,var3         # load var3 to $t3
            lw $t4,var4         # load var4 to $t4
            la $t5,first        # load first to $t5
            la $t6,last         # load last to $t6

            sw $t4,var1         # swap var1 & var 4
            sw $t1,var4
            sw $t3,var2         # swap var2 & var3
            sw $t2,var3
             

            
# restore now the return address in $ra and return from main
            addu $ra, $0, $s0       # return address back in $31
            jr $ra                  # return from main
